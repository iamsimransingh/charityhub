<?php require 'header_files.php'; ?>
<body>
<script>
	$(document).ready(function()
	{
		$('#manageproject').DataTable();
	});
</script>
<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;"> Manage Project </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped" id="manageproject">
				<thead>
					<tr>
						<th>Project ID</th>
						<th>Project Name</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Project ID</th>
						<th>Project Name</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					<?php
					$did=$_SESSION['d_id'];
					$q="select * from add_project where project_upload_by='$did'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
					?>
					<tr>
						<td><?php echo $r['project_id']; ?></td>
						<td><?php echo $r['project_name']; ?></td>
						<td><?php echo $r['project_date']; ?></td>
						<td><?php echo $r['project_end_date']; ?></td>
						<td><?php echo $r['time']; ?></td>
						<td>
						<a href="view_project.php?z=<?php echo $r['project_id']; ?>" class="btn btn-xs btn-info">View</a>
						<a href="delete_project.php?z=<?php echo $r['project_id']; ?>" class="btn btn-xs btn-danger">Delete</a>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>
</body>
</html>
