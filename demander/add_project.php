<?php require 'header_files.php'; 
session_start();
error_reporting('ERROR');
	if(isset($_REQUEST['sub']))
	{
		$a=$_REQUEST['pname'];
		$b=$_REQUEST['sdate'];
		$c=$_REQUEST['edate'];
		$d=$_REQUEST['loc'];
		$e=$_REQUEST['addr'];
		$f=$_REQUEST['lati'];
		$g=$_REQUEST['long'];
		$h=$_REQUEST['mini'];
		$i=$_REQUEST['maxi'];
		$n=$_REQUEST['phn'];
		$j=$_REQUEST['cate'];
		$k=$_FILES['upl']['name'];
		$l=$_REQUEST['des'];
		$m="disable";
		$upload=$_SESSION['d_id'];
		
		
		// allowed Extension
		$allowedExts_cimg = array("jpg", "png","jpeg","JPG","PNG","JPEG");
		$extension_cimg = end(explode(".",$k));  //jpg
		
			foreach($allowedExts_cimg as $arrimg)
			{
				if($arrimg==$extension_cimg)
				{
					$q="insert into add_project(project_name,project_date,project_end_date,project_location,project_address,project_latitute,project_longitute,project_mini,project_maxi,project_phone,project_category,project_image,project_description,project_status,project_upload_by,time)values('$a','$b','$c','$d','$e','$f','$g','$h','$i','$n','$j','$k','$l','$m','$upload',NOW())";
				
					if($conn->query($q))
					{
						move_uploaded_file($_FILES['upl']['tmp_name'],"../admin/upload_image_project/".$k);
						$success="Project Inserted Successfully!!!";
					}
					else
					{
						$failure="Try Again!!!" . $conn->connect_Error;
					}
				
			}
					else
					{
						$failure="Upload Valid Type of file!!!<br> Cover:JPG,JPEG,PNG,jpg,jpeg,png";
					}
				}
	}

?>
<body>

<script>
function f1()
{
	var a=document.getElementById("pname").value;
	var min=parseInt(document.getElementById("mind").value);
	var max=parseInt(document.getElementById("maxd").value);
	if (! isNaN(a))
	{
		alert("Invalid name!! Digits not Allowed!!");
		return false;
	}
	if(max < min)
	{
		alert("Maximum demand is less than Minimum!!");
		return false;
	}
	
}
</script>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            	 <h2><p style="color:green;"> <?php echo $success; ?></p>
				<p style="color:red;"> <?php echo $failure; ?></p></h2>
            <!-- ... Your content goes here ... -->
					<form method="post" enctype="multipart/form-data">
				<div class="form-group" name="project">
					<label><b style="font-family:Andalus; font-size:33px;text-align:center;"> Add Project </b></label><br>
					Project Name <input type="text" class="form-control" id="pname" name="pname" required><br>
					Start Date <input type="date" name="sdate" class="form-control" required><br>
					End Date <input type="date" name="edate" class="form-control" required><br>
					Location <input type="text" name="loc" class="form-control" required><br>
					Address<textarea rows="5" cols="5" name="addr" id="address" class="form-control" required></textarea><br>
					Latitute <input type="text" name="lati" class="form-control" required><br>
					Longitute <input type="text" name="long" class="form-control" required><br>
					Minimum Demand <input type="text" name="mini" id="mind" class="form-control" required><br>
					Maximum Demand <input type="text" name="maxi" id="maxd" class="form-control" required><br>
					Phone No.<input type="text" class="form-control" name="phn" required><br>
					Category <select  class="form-control" name="cate" required>
							<?php
								$q="select * from add_category";
								$chk=$conn->query($q);
								while($r=$chk->fetch_assoc())
								{
								?>
									<option value="<?php echo $r['category_name']; ?>">
									<?php echo $r['category_name']; ?>
									</option>
								<?php
								}
								?>
								</select><br>
							
					Uplaod Image <input type="file" class="form-control" name="upl" required><br>
					Description<textarea rows="5" cols="5" id="des" name="des" class="form-control" required></textarea><br>
					<br>
					<input type="submit" value="Submit" name="sub" class="btn btn-primary" onclick="return(f1())">
				</div>
			</form>
                			
			<!-- ... Your content end here ... -->
        </div>
    </div>

</div>
</body>
</html>