<?php
session_start();
if($_SESSION==NULL)
{
	header("location:index.php");
}
?>

 
<nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><b style="font-family:Andalus; font-size:32px;">Charity Navigators</b></a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

       <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['d_name']; ?>  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="user_profile.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="change_password.php"><i class="fa fa-gear fa-fw"></i>Change Password</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    
                    <li>
                        <a href="main.php" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="funds.php" class="active"><i class="fa fa-dashboard fa-fw"></i> Funds Received</a>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-desktop"></i> Project<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="add_project.php"><i class="fa fa-plus-circle"></i> Add Project</a>
                            </li>
                            <li>
                                <a href="manage_project.php"><i class="fa fa-chain-broken"></i> Manage Project</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-bars"></i> Message<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="compose_message.php"><i class="fa fa-plus"></i> Compose Message</a>
                            </li>
                            <li>
                                <a href="inbox.php"><i class="fa fa-chain-broken"></i> Inbox</a>
                            </li>
                        </ul>
                    </li>
					
				</ul>

            </div>
        </div>
    </nav>