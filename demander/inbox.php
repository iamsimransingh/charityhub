<?php require 'header_files.php'; ?>
<body>
<script>
	$(document).ready(function(){
		$('#manageadmin').DataTable();

	});
</script>
<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;"> Manage Reviews </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped" id="manageadmin">
				<thead>
							<tr>
								<th align="center">From</th>
								<th align="center">Subject</th>
								<th colspan="2" align="center"> Actions</th>
							</tr>
					</thead>
					<tfoot>
							<tr>
								<th align="center">From</th>
								<th align="center">Subject</th>
								<th colspan="2" align="center"> Actions</th>
							</tr>
					</tfoot>
					<tbody>	
			
                        <?php
						$user=$_SESSION['d_id']; $f=0;
						$q="select * from compose_message where c_to='$user' ORDER BY Id Desc";
						$c= $conn->query($q);
						while($r=$c->fetch_assoc())
						{
			               ?>
							<tr>
								
								<td><?php echo $r['c_from']; ?></td>
								<td><?php echo $r['c_subject']; ?></td>
                                
                                 
                                 <td> <a class="btn btn-primary btn-xs" href= "msg_view.php?z=<?php echo $r['Id'];?>">View</a> 
							
								 <a class="btn btn-danger btn-xs" href="delete_msg.php?z=<?php echo $r['Id'];?>">Delete</a></td>
                                
							</tr>
                            <?php 
							$f=1;
							} 
							if($f==0)
							{
							?>
							<tr>
								
								<td colspan="4">No Message Found !!!</td>
							</tr>
							<?php
							}
							?>
                    </tbody>
			</table>
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>
</body>
</html>
