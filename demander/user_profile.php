<?php require 'header_files.php'; ?>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;">My Profile</b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table">
				<?php
				
				$id=$_SESSION['d_id']; 
				if($id=="")
				{
					echo "<script>alert('Invalid ID'); window.location='manage_admin.php';</script>";
				}
				else
				{
					$q="select * from add_user where user_id='$id'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
				?>
						<tr>
							<th>ID</th>
							<td><?php echo $r['user_id']; ?></td>
						</tr>
						<tr>
							<th>First Name</th>
							<td><?php echo $r['user_fname']; ?></td>
						</tr>
						<tr>
							<th>Last Name</th>
							<td><?php echo $r['user_lname']; ?></td>
						</tr>
						<tr>
							<th>Email ID</th>
							<td><?php echo $r['user_email']; ?></td>
						</tr>
						<tr>
							<th>Image</th>
							<td><img src="../admin/upload_image_user/<?php echo $r['user_image'];?>" height="100px" width="200px"></td>
						</tr>
						<tr>
							<th>Gender</th>
							<td><?php echo $r['user_gender']; ?></td>
						</tr>
						<tr>
							<th>Address</th>
							<td><?php echo $r['user_address']; ?></td>
						</tr>
						
						<tr>
							<th>Registered Time</th>
							<td><?php echo $r['time']; ?></td>
						</tr>
						
				<?php
					}
				}
				?>
			</table>
        </div>
    </div>
</div>
</body>
</html>
