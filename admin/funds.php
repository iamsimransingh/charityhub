<?php require 'header_files.php'; ?>
<body>
<script>
	$(document).ready(function()
	{
		$('#manageproject').DataTable();
	});
</script>
<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;">Funds Collected </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped" id="manageproject">
				<thead>
					<tr>
						<th>ID</th>
						<th>Project Name</th>
						<th>Fund Received</th>
						<th>Remaining Days Left</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Project Name</th>
						<th>Fund Received</th>
						<th>Remaining Days Left</th>
					</tr>
				</tfoot>
				<tbody>
					<?php
					$i=1;
					$q="select * from add_project";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{ $id=$r['project_id'];
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $r['project_name']; ?></td>
						<td style="color:red;">
							<?php	
							$d=0;
							$q="select SUM(raised_fund) as tot from raise_funds where pid='$id'";
							$sum=$conn->query($q);
							while ($test = $sum->fetch_assoc()) 
							 {  if($test['tot']==0){echo "$0"; }else{echo "$ ".$test['tot'];} }
							?>
						</td>
						<td>
						<?php 
							$today = time();
							$remaining=strtotime($r['project_end_date'])- $today;
							$days_remaining=floor($remaining / 86400);
							 echo $days_remaining." days"; 
						?>
						</td>
					</tr>
					<?php
					$i++;
					}
					?>
				</tbody>
			</table>
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>
</body>
</html>
