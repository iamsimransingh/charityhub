<?php require 'header_files.php'; ?>
<body>
<script>
	$(document).ready(function(){
		$('#managecategory').DataTable();
		
	});
</script>
<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;"> Manage Category </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped" id="managecategory">
				<thead>
					<tr>
						<th>Category ID</th>
						<th>Category Name</th>						
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Category ID</th>
						<th>Category Name</th>
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					<?php
					$q="select * from add_category";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
					?>
					<tr>
						<td><?php echo $r['category_id']; ?></td>
						<td><?php echo $r['category_name']; ?></td>						
						<td><?php echo $r['time']; ?></td>
						<td>
						<a href="#" class="btn btn-xs btn-info">View</a>
						<a href="edit_category.php?z=<?php echo $r['category_id']; ?>" class="btn btn-xs btn-warning">Edit</a>
						<a href="delete_category.php?z=<?php echo $r['category_id']; ?>" class="btn btn-xs btn-danger">Delete</a>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>
</body>
</html>
