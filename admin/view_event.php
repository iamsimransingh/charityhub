<?php require 'header_files.php'; ?>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:37px;text-align:center;"> Welcome To View Panel </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped">
				<?php
				$id=$_GET['z'];
					if($id=="")
						{
							echo "<script>alert('Invalid ID!!'); window.location='manage_event.php';</script>";
						}
						else
						{
							$q="select * from add_event where event_id='$id'";
							$chk=$conn->query($q);
							while($r=$chk->fetch_assoc())
							{
				?>
				<tr>
					<th>Event ID</th>
					<td><?php echo $r['event_id'];?></td>
				</tr>
				<tr>
					<th>Event Name</th>
					<td><?php echo $r['event_name'];?></td>
				</tr>
				<tr>
					<th>Description</th>
					<td><?php echo $r['event_description'];?></td>
				</tr>
				<tr>
					<th>Location</th>
					<td><?php echo $r['event_location'];?></td>
				</tr>
				<tr>
					<th>Upload Image</th>
					<td><?php echo $r['event_image'];?></td>
				</tr>
				<tr>
					<th>Starting Date</th>
					<td><?php echo $r['event_sdate'];?></td>
				</tr>
				<tr>
					<th>Registration Time</th>
					<td><?php echo $r['time'];?></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><a href="manage_event.php" class="btn btn-primary">Go To Previous Page</a></td>
				</tr>
				<?php
					}
				}
				?>
			</table>
			
			
			
			
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>



</body>
</html>
