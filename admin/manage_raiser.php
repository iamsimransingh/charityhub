<?php require 'header_files.php'; ?>
<body>
<script>
	$(document).ready(function(){
		$('#manageuser').DataTable();

	});
</script>
<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:33px;text-align:center;"> Manage Raiser </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped" id="manageuser">
				<thead>
					<tr>
						<th>user ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email-ID</th>
						<th>Address</th>
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>user ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email-ID</th>
						<th>Address</th>
						<th>Registered Time</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					<?php
					$q="select * from add_user where user_type='raiser'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
					?>
					<tr>
						<td><?php echo $r['user_id']; ?></td>
						<td><?php echo $r['user_fname']; ?></td>
						<td><?php echo $r['user_lname']; ?></td>
						<td><?php echo $r['user_email']; ?></td>
						<td><?php echo $r['user_address']; ?></td>
						<td><?php echo $r['time']; ?></td>
						<td>
						<a href="view_raiser.php?z=<?php echo $r['user_id']; ?>" class="btn btn-xs btn-info">View</a>
						<a href="edit_raiser.php?z=<?php echo $r['user_id']; ?>" class="btn btn-xs btn-warning">Edit</a>
						<a href="delete_raiser.php?z=<?php echo $r['user_id']; ?>" class="btn btn-xs btn-danger">Delete</a>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>



</body>
</html>
