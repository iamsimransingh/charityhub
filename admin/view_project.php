<?php require 'header_files.php'; ?>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <b style="font-family:Andalus; font-size:37px;text-align:center;"> View Project </b><br><br>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			<table class="table table-bordered table-striped">
				<?php
				$id=$_GET['z'];
					if($id=="")
						{
							echo "<script>alert('Invalid ID!!'); window.location='manage_project.php';</script>";
						}
						else
						{
							$q="select * from add_project where project_id='$id'";
							$chk=$conn->query($q);
							while($r=$chk->fetch_assoc())
							{
				?>
				<tr>
					<th colspan="2">
					<?php 
						$st=$r['project_status'];
						if($st=="enable")
						{
							echo "<h1 style='color:green;'>Project is Enable !!!</h1>";
						}
						else
						{
							echo "<h2 style='color:red;'>Project Under Verification!!! Soon Your Project Will Be Enable!!!</h2>";
						}
					?>
					</th>
				</tr>
				<tr>
					<th>Project ID</th>
					<td><?php echo $r['project_id'];?></td>
				</tr>
				<tr>
					<th>Project Name</th>
					<td><?php echo $r['project_name'];?></td>
				</tr>
				<tr>
					<th>Project Starting Date</th>
					<td><?php echo $r['project_date'];?></td>
				</tr>
				<tr>
					<th>Project End Date</th>
					<td><?php echo $r['project_end_date'];?></td>
				</tr>
				<tr>
					<th>Location</th>
					<td><?php echo $r['project_location'];?></td>
				</tr>
				<tr>
					<th>Address</th>
					<td><?php echo $r['project_address'];?></td>
				</tr>
				<tr>
					<th>Location</th>
					<td>
					<!-- location code -->
							<div style="height:350px; width:350px;">
							 <?php 
								$source =  $r['project_latitute'].' , '.$r['project_longitute'];
							?>
							<iframe id="mapFrame" width="350px" height="250px" style="float:left;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?saddr=<?php echo $source; ?>+&amp;ie=UTF8&amp;z=12&amp;t=m&amp;output=embed"></iframe>
							</div>
							<!-- location code end-->
					</td>
				</tr>
				<tr>
					<th>Minimum Demand</th>
					<td><?php echo $r['project_mini'];?></td>
				</tr>
				<tr>
					<th>Maximum Demand</th>
					<td><?php echo $r['project_maxi'];?></td>
				</tr>
				<tr>
					<th>Phone No.</th>
					<td><?php echo $r['project_phone'];?></td>
				</tr>
				<tr>
					<th>Category</th>
					<td><?php echo $r['project_category'];?></td>
				</tr>
				<tr>
					<th>Upload Image</th>
					<td><img src="upload_image_project/<?php echo $r['project_image'];?>" height="100px" width="200px"></td>
				</tr>
				<tr>
					<th>Description</th>
					<td><?php echo $r['project_description'];?></td>
				</tr>
				
				<tr>
					<th>Registration Time</th>
					<td><?php echo $r['time'];?></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><a href="manage_project.php" class="btn btn-primary">Go To Previous Page</a></td>
				</tr>
				<?php
					}
				}
				?>
			</table>
			
			
			
			
			<!-- ... Your content end here ... -->

        </div>
    </div>

</div>



</body>
</html>
