<?php require 'header_files.php'; ?>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Welcome To Your Account !!!</h1>
                </div>
            </div>

            <!-- ... Your content goes here ... -->
			
			<!-- ... 2nd row ... -->
			<div class="col-lg-12" style="margin-top:30px;">
                <div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/admin.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_admin.php" class="btn btn-danger active">Total Admin:
					<?php
						$x="select * from add_admin";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div> 
				 <div class="col-lg-1"></div>
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/asi-icon-08.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_demander.php" class="btn btn-danger active">Total Demanders:
					<?php
						$x="select * from add_user where user_type='demander'";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/icon-donor-connect.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_raiser.php" class="btn btn-danger active">Total Raisers:
					<?php
						$x="select * from add_user where user_type='demander'";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div>
			
			</div>
			<!-- ... 2nd row end... -->
			
			<!-- ... 2nd row ... -->
			<div class="col-lg-12" style="margin-top:30px;">
				<div class="col-lg-2"></div>
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/Support.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_project.php" class="btn btn-danger active">Total Enable Projects: 
					<?php
						$x="select * from add_project where project_status='enable'";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div> 
				
				<div class="col-lg-1"></div>
				
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/Alert.ico" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_project_d.php" class="btn btn-danger active">Total Disable Projects:  
					<?php
						$x="select * from add_project where project_status='disable'";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div> 
				
            </div>
			<!-- ... 2nd row end... -->
			
			
			 <!-- ... 1st row ... -->
			<div class="col-lg-12" style="margin-top:30px;">
			
                <div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/Category_Icon.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_category.php" class="btn btn-danger active">Total Categories:
					<?php
						$x="select * from add_category";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?>
					
					</a>
					<br><br>
				</div> 
				 <div class="col-lg-1"></div>
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/images.jpg" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="manage_event.php" class="btn btn-danger active">Total Events: 
					<?php
						$x="select * from add_event";
						$chk1=$conn->query($x);
						$row=$chk1->num_rows;
						echo $row;
					?></a>
					<br><br>
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-3" align="center" style="box-shadow:1px 1px 5px gray; border-radius:10px;">
					<br>
						<img src="images/asi-icon-10.png" class="img-circle" style="height:150px; width:150px; box-shadow:1px 1px 5px gray">
					<br><br>
					<a href="funds.php" class="btn btn-danger active">Total Funds Raised</a>
					<br><br>
				</div>
			
			</div>
			<!-- ... 1st row end ... -->
			
			
			
			
        </div>
    </div>

</div>



</body>
</html>
