<?php require 'header_files.php';
	error_reporting('ERROR');
	if(isset($_REQUEST['sub']))
	{
		$a=$_REQUEST['ename'];
		$b=$_REQUEST['des'];
		$c=$_REQUEST['loc'];
		$d=$_REQUEST['addr'];
		$e=$_FILES['upl1']['name'];
		$f=$_REQUEST['sdate'];
		$g=$_REQUEST['etime'];
		
		// allowed Extension
	$allowedExts_cimg = array("jpg", "png","jpeg");
    $extension_cimg = end(explode(".",$e));//jpg
		
		foreach($allowedExts_cimg as $arrimg)
		{
			if($arrimg==$extension_cimg)
			{
				$q="insert into add_event(event_name,event_description,event_location,event_address,event_image,event_sdate,event_etime,time)values('$a','$b','$c','$d','$e','$f','$g',NOW())";
				if($conn->query($q))
				{
					move_uploaded_file($_FILES['upl1']['tmp_name'],"upload_image_event/".$e);
					$success="Event Inserted Successfully!!!";
				}
				else
				{
					$failure="Try Again!!!" . $conn->connect_Error;
				}
		    }
			else
			{
				$failure="Upload Valid Type of file!!!<br> Cover: jpg,jpeg,png";
			}
		}
	}
 ?>
<body>

<script>
function f1()
{
	var a=document.getElementById("ename").value;
	if (! isNaN(a))
	{
		alert("Invalid name!! Digits not Allowed!!");
		return false;
	}
	
}
</script>

<div id="wrapper">

    <!-- Navigation -->
    <?php require 'menu.php'; ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
			<h2><p style="color:green;"> <?php echo $success; ?></p>
			<p style="color:red;"> <?php echo $failure; ?></p></h2>

            <!-- ... Your content goes here ... -->
			<form method="post" enctype="multipart/form-data" name="event">
				<div class="form-group">
					<label><b style="font-family:Andalus; font-size:33px;text-align:center;"> Add Event </b></label><br>
					Event Name <input type="text" class="form-control" id="ename" name="ename" required><br>
					Description<textarea rows="5" cols="5" id="des" name="des" class="form-control" required></textarea><br>
					Location <input type="text" class="form-control" name="loc" required><br>
					Address<textarea rows="5" cols="5" id="address" name="addr"class="form-control" required></textarea><br>
					Uplaod Image <input type="file" name="upl1" class="form-control" required><br>
					Start Date <input type="date" name="sdate" class="form-control" required><br>
					Event Time <input type="time" name="etime" class="form-control" required><br>
					
					<input type="submit" value="Submit" name="sub" class="btn btn-primary" onclick="return(f1())">
				</div>
			</form>

		<!-- ... Your content end here ... -->
        </div>
    </div>

</div>
</body>
</html>
