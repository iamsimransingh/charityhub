
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2017 at 08:20 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u180035249_mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_admin`
--

CREATE TABLE IF NOT EXISTS `add_admin` (
  `admin_id` int(100) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(200) NOT NULL,
  `admin_email` varchar(200) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `add_admin`
--

INSERT INTO `add_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `time`) VALUES
(1, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '2017-04-10 17:42:52'),
(2, 'navneet', 'navneet@gmail.com', 'd72e5ee9b367833956ed9f88a960c686', '2017-04-10 17:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `add_category`
--

CREATE TABLE IF NOT EXISTS `add_category` (
  `category_id` int(100) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `add_category`
--

INSERT INTO `add_category` (`category_id`, `category_name`, `time`) VALUES
(1, 'Children having intellactual disability', '2017-05-06 08:17:35'),
(2, 'Orphanage', '2017-05-06 08:18:15'),
(3, 'For girl child', '2017-05-06 08:18:37'),
(4, 'Disaster relief', '2017-05-06 08:18:52'),
(5, 'Street children education', '2017-05-06 08:19:25'),
(6, 'For old age home', '2017-05-06 08:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `add_event`
--

CREATE TABLE IF NOT EXISTS `add_event` (
  `event_id` int(100) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(200) NOT NULL,
  `event_description` varchar(200) NOT NULL,
  `event_location` varchar(200) NOT NULL,
  `event_address` varchar(200) NOT NULL,
  `event_image` varchar(400) NOT NULL,
  `event_sdate` varchar(200) NOT NULL,
  `event_etime` varchar(200) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `add_event`
--

INSERT INTO `add_event` (`event_id`, `event_name`, `event_description`, `event_location`, `event_address`, `event_image`, `event_sdate`, `event_etime`, `time`) VALUES
(13, 'smile foundation', 'Smile Foundation has been awarded the NTR Memorial Trust Award for its outstanding achievement and high standards of excellence in the implementation of its educational programme for underprivileged c', 'Delhi', 'new Delhi.', 'img9.jpg', '2017-05-19', '9AM TO 10PM', '2017-05-08 08:34:18'),
(14, 'Amar Jyoti Charitable Trust', 'Working for the development of the less privileged has always been an important aspect of the way we function and work. Hence when Smile Foundation approached us with the concept of having an integrat', 'chandigarh', '3B2 Mohali.', 'img4.jpg', '2017-05-19', '9AM TO 10PM', '2017-05-08 08:36:54'),
(15, 'Blind PeopleÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢s Association', 'In order that corporate can connect with good quality and well established Implementing Agencies, IICA has set up a CSR Implementing Agency (IA) Hub. This would seek to meet the urgent requirement by ', 'chandigarh', '3b2 Mohali.', 'img3.jpg', '2017-05-19', '9AM TO 10PM', '2017-05-08 08:38:55');

-- --------------------------------------------------------

--
-- Table structure for table `add_project`
--

CREATE TABLE IF NOT EXISTS `add_project` (
  `project_id` int(100) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) NOT NULL,
  `project_date` varchar(200) NOT NULL,
  `project_end_date` varchar(200) NOT NULL,
  `project_location` varchar(200) NOT NULL,
  `project_address` varchar(200) NOT NULL,
  `project_latitute` varchar(200) NOT NULL,
  `project_longitute` varchar(200) NOT NULL,
  `project_mini` varchar(200) NOT NULL,
  `project_maxi` varchar(200) NOT NULL,
  `project_phone` int(200) NOT NULL,
  `project_category` varchar(200) NOT NULL,
  `project_image` varchar(400) NOT NULL,
  `project_description` varchar(200) NOT NULL,
  `project_status` varchar(200) NOT NULL,
  `project_upload_by` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `add_project`
--

INSERT INTO `add_project` (`project_id`, `project_name`, `project_date`, `project_end_date`, `project_location`, `project_address`, `project_latitute`, `project_longitute`, `project_mini`, `project_maxi`, `project_phone`, `project_category`, `project_image`, `project_description`, `project_status`, `project_upload_by`, `time`) VALUES
(16, 'charityfunding', '2017-05-20', '2017-08-30', 'chandigarh', '3b2 mohali', '30.712254', '76.720665', '10', '120', 2147483647, 'Children having intellactual disability', 'img8.jpg', 'plz do charity', 'enable', 'admin', '2017-05-10 07:40:52'),
(17, 'child home', '2017-05-19', '2018-05-19', 'chandigarh', 'SCF 21, Mohali Stadium Road, Phase 3B2, Sector 60, Sahibzada Ajit Singh Nagar, Punjab 160059', '30.712254', '76.720665', '10', '78', 2147483647, 'Orphanage', 'img7.jpg', 'please  do charity.', 'enable', 'admin', '2017-05-10 07:41:03'),
(18, 'jan sewa', '2017-05-19', '2018-05-19', 'parade ground', 'Jammu and Kashmir 180001', '32.737242', '74.866795', '20', '240', 2147483647, 'For girl child', 'img2.jpg', 'Old Heritage City', 'disable', 'admin', '2017-05-10 07:41:31'),
(19, 'jan sewa', '2017-05-19', '2018-05-19', 'Jammu', 'Jammu and Kashmir 180001', '32.737242', '74.866795', '15', '240', 2147483647, 'Street children education', 'img10.jpg', 'Help poor people', 'enable', 'admin', '2017-05-10 07:41:37'),
(20, 'smile foundation', '2017-05-19', '2018-05-19', 'chandigarh', '3B2 mohali', '30.712254', '76.720665', '20', '200', 2147483647, 'For old age home', 'img5.jpg', 'Help poor people.', 'disable', 'admin', '2017-05-10 07:41:49'),
(21, 'smile foundation', '2017-05-19', '2018-05-19', 'Chandigarh', '17 sector ,Chandigarh', '30.712254', '76.720665', '20', '100', 2147483647, 'Children having intellactual disability', 'img3.jpg', 'help poor people.', 'disable', '4', '2017-05-10 07:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `add_user`
--

CREATE TABLE IF NOT EXISTS `add_user` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(200) NOT NULL,
  `user_lname` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `user_image` varchar(400) NOT NULL,
  `user_gender` varchar(200) NOT NULL,
  `user_address` varchar(200) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `add_user`
--

INSERT INTO `add_user` (`user_id`, `user_fname`, `user_lname`, `user_email`, `user_password`, `user_image`, `user_gender`, `user_address`, `user_type`, `time`) VALUES
(1, 'navneet', 'Grewal', 'navneet@gmail.com', 'd72e5ee9b367833956ed9f88a960c686', 'Desert.jpg', 'female', 'Ldh', 'raiser', '2017-03-22 05:03:51'),
(2, 'sandeep', 'kaur', 'sandeep@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '6.jpg', 'female', 'stb Barnala', 'raiser', '2017-05-08 08:49:50'),
(3, 'kamalpreet', 'kaur', 'kamal123@gmail.com', '202cb962ac59075b964b07152d234b70', 'img10.jpg', 'female', 'VPO Thuliwal Distt.Barnala.', 'raiser', '2017-05-08 08:50:47'),
(4, 'Hardeep', 'Kaur', 'hardeep123@gmail.com', '202cb962ac59075b964b07152d234b70', 't5.png', 'female', 'VPO Balian Distt Sangrur.', 'demander', '2017-05-09 05:26:11'),
(5, 'Manpreet', 'Kaur', 'manipreet@gmail.com', '202cb962ac59075b964b07152d234b70', 't2.png', 'female', 'VPO Pohir Distt Ludhiana.', 'demander', '2017-05-09 05:26:27'),
(8, 'Mandeep', 'Singh', 'mandeep123@gmail.com', '202cb962ac59075b964b07152d234b70', 't9.jpg', 'male', '3B2 Mohali', 'demander', '2017-05-09 05:28:46');

-- --------------------------------------------------------

--
-- Table structure for table `compose_message`
--

CREATE TABLE IF NOT EXISTS `compose_message` (
  `Id` int(200) NOT NULL AUTO_INCREMENT,
  `c_to` varchar(200) NOT NULL,
  `c_from` varchar(200) NOT NULL,
  `c_subject` text NOT NULL,
  `c_message` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `compose_message`
--

INSERT INTO `compose_message` (`Id`, `c_to`, `c_from`, `c_subject`, `c_message`, `time`) VALUES
(3, '1', 'admin', 'regarding appriciating us', 'thnku navneet.. ', '2017-04-10 19:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `raise_funds`
--

CREATE TABLE IF NOT EXISTS `raise_funds` (
  `fund_id` int(4) NOT NULL AUTO_INCREMENT,
  `user_id` int(4) NOT NULL,
  `raised_fund` int(10) NOT NULL,
  `pid` int(200) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fund_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `raise_funds`
--

INSERT INTO `raise_funds` (`fund_id`, `user_id`, `raised_fund`, `pid`, `time`) VALUES
(31, 1018, 1234, 3, '2017-03-30 04:44:45'),
(32, 1018, 10000, 342, '0000-00-00 00:00:00'),
(33, 1026, 1000, 15, '2017-04-10 17:22:06'),
(34, 1026, 0, 3, '2017-03-30 04:44:56'),
(35, 1026, 23232, 15, '2017-04-10 17:22:01'),
(36, 1026, 23232, 3, '2017-03-30 04:44:51'),
(37, 1, 2, 8, '2017-04-04 05:52:13'),
(38, 1, 3, 8, '2017-04-10 05:52:14'),
(39, 3, 1000, 17, '2017-05-10 07:08:29'),
(40, 3, 1100, 16, '2017-05-10 07:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_contact`
--

CREATE TABLE IF NOT EXISTS `user_contact` (
  `contact_id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user_contact`
--

INSERT INTO `user_contact` (`contact_id`, `name`, `email`, `subject`, `message`) VALUES
(11, 'navneet', 'navneetkaurjagait@gmail.com', 'navneetkaurjagait@gmail.com', 'Hi, admin .. i also want to donate. can u explain me the procedure on my mail id.'),
(12, 'navneet', 'navneetkaurjagait@gmail.com', 'my donation', 'Hi, admin .. i also want to donate. can u explain me the procedure on my mail id.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
