<?php require "header_files.php"; 
?>
<body> 
	<?php require "header_al.php"; ?>
<div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
			<div class="col-lg-3"></div>
                <div class="col-lg-8">
				<br><br>
                    <b style="font-family:Andalus; font-size:33px;text-align:center;">My Profile</b><br><br>
                </div>
			<div class="col-lg-2"></div>
            </div>

            <!-- ... Your content goes here ... -->
			<div class="col-md-3"></div>
			<div class="col-md-7">
			<table class="table">
				<?php
				
				$id=$_SESSION['r_id']; 
				if($id=="")
				{
					echo "<script>alert('Invalid ID'); window.location='myaccount.php';</script>";
				}
				else
				{
					$q="select * from add_user where user_id='$id'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
				?>
						
						<tr>
							<th>Name</th>
							<td><?php echo $r['user_fname']." ".$r['user_lname']; ?></td>
							<td rowspan="5">
							<img src="admin/upload_image_user/<?php echo $r['user_image']; ?>" height="250px" width="250px" class="img-thumbnail" style="box-shadow:1px 1px 10px #00bcd4;" >
							<br><br>
							<h2>Hi, <?php echo $r['user_fname']." ".$r['user_lname']; ?></h2>
							</td>
						</tr>
						
						<tr>
							<th>Email ID</th>
							<td><?php echo $r['user_email']; ?></td>
						</tr>
						
						<tr>
							<th>Gender</th>
							<td><?php echo $r['user_gender']; ?></td>
						</tr>
						<tr>
							<th>Address</th>
							<td><?php echo $r['user_address']; ?></td>
						</tr>
						
						<tr>
							<th>Registered Time</th>
							<td><?php echo $r['time']; ?></td>
						</tr>
						
				<?php
					}
				}
				?>
			</table>
			</div>
			<div class="col-md-2"></div>
        </div>
    </div>
	<br><br><br><br>
	<?php require "footer.php"; ?>
</body>
</html>