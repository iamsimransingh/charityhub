<?php require "header_files.php"; ?>
</head>
<body> 
	<!-- header -->
	<?php require "header.php"; ?>
	<!-- //header -->
	
	<!-- Our Projects -->
	<!-- about -->
	<div class="about w3-agileits">
		<div class="container">
			<h3 class="agileits-title1">About Us</h3> 
			<div class="col-md-4 about-wthreegrids">
				<h3 class="w3ltitle">WHAT WE ARE </h3>
				<p><b>Charity Hub </b>is one of the online funding process whereby demanders can directly demand for fund money in real-time without an intermediary service over the Internet. If an intermediary service is present the process is called electronic commerce. Also fund releaser can directly release fund money to any demander's fund.</p>
				<p>The inputs of the individuals in the charity trigger the charity funding process and influence the ultimate value of the offerings or outcomes of the process. Each individual acts as an agent of the offering, selecting and promoting the projects in which they believe. They will sometimes play a donor role oriented towards providing help on social projects. In some cases they will become shareholders and contribute to the development and growth of the offering.</p>
			</div>
			<div class="col-md-4 about-wthreegrids">
				<img src="images/img3.jpg" alt=""/>
			</div>
			<div class="col-md-4 about-wthreegrids">
				<h3 class="w3ltitle">OUR CATEGORIES</h3>
				<!-- loop start -->
				<?php
					$q="select * from add_category"; $i=1;
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
				?>
				<a href="project_cat_bl.php?z=<?php echo $r['category_name']; ?>">
				<div class="w3ls-pince agileits-w3layouts">
					<div class="pince-left">
						<h5><?php echo $i; ?></h5>
					</div>
					<div class="pince-right">
						<h4><?php echo $r['category_name']; ?></h4>
					</div>
					<div class="clearfix"> </div>
				</div>
				</a>
				<?php
					$i++; }
				?>
				<!-- loop end -->
				
				
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->
	<!-- about-slid -->
	<div class="about-slid agileits-w3layouts"> 
		<div class="container">
			<div class="about-slid-w3info"> 
				<iframe width="100%" height="515" src="https://www.youtube.com/embed/a4z7JtMtJUg" frameborder="0" allowfullscreen></iframe>
				
			</div>
		</div>
	</div>
	<!-- //about-slid -->
	<!-- team -->
	<div class="team w3-agileits">
		<div class="container">  
			<h3 class="agileits-title1">Our Requester</h3>  
			<div class="team-w3ls-row">
				
				<!-- loop start -->
				<?php
				$per_page=4;
				if(isset($_GET["page"])) 
				{
					$page=$_GET["page"];
				}
				else {	$page=1; }
				// Page will start from 0 and Multiple by Per Page
				$start_from =($page-1)*$per_page;

					$type="demander";
					$q="select * from add_user where user_type='$type' LIMIT $start_from, $per_page";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
				?>
				<div class="col-md-3 col-sm-6 team-grids">
					<img src="admin/upload_image_user/<?php echo $r['user_image']; ?>" alt="" width="260px" height="300px"/>
					<div class="img-caption w3-agileits">
						<div class="img-agileinfo-text">
							<h4><?php echo $r['user_fname']." ".$r['user_lname']; ?></h4>
							<p><b>Adddress: </b><?php echo $r['user_address']; ?></p>
							
						</div>
					</div>
				</div>
				
				<?php
					}
				?>
				<!-- loop end -->
				
				
				
				
				<div class="clearfix"> </div> 
			</div>
<div style="float:right; margin-top:15px;">
<?php
//Now select all from table
$query = "select * from add_user where user_type='$type'";
$result = $conn->query($query);
// Count the total records
$total_records = $result->num_rows;

//Using ceil function to divide the total records on per page
$total_pages = ceil($total_records / $per_page);

//Going to first page
for($i=1; $i<$total_pages; $i++) 
{
	echo "<a class='mybtn' href='about.php?page=".$i."'>".$i."</a>";
}
// Going to last page
echo "<a class='mybtn' href='about.php?page=$total_pages'>".'Last Page'."</a>";
?>
</div>		
		</div>
	</div>
	
	<!-- //team --> 
	<!-- //our projects --> 
	<!-- footer -->
	<?php require "footer.php"; ?>
	<!-- //footer -->  
	<!-- start-smooth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>	
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	
	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
</body>
</html>