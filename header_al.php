<?php
session_start();
if($_SESSION==NULL)
{
	header("location:index.php");
}
?>
<div class="headerw3l">
	<nav class="navbar navbar-default">
		<div class="container">
				<div class="navbar-header navbar-left">
					<h1><a href="index.html">Charity Hub <span><i>Learn.</i> <i class="logo-w3l2">Share.</i> <i class="logo-w3l3"> Laugh.</i> <i class="logo-w3l4"> Grow.</i></span></a></h1>
				</div>
				<!-- navigation --> 
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button> 
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">					
					<ul class="nav navbar-nav navbar-left">
						<li class="active"><a href="myaccount.php" class="link link--yaku"><span>H</span><span>i</span><span>, <?php echo $_SESSION['r_name']; ?></span></a></li>
						
						<li><a href="project.php" class="link link--yaku"><span>P</span><span>R</span><span>O</span><span>J</span><span>E</span><span>C</span><span>T</span><span>S</span></a></li>
					
						<li><a href="#" class="dropdown-toggle link link--yaku" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span>M</span><span>Y</span><span>&nbsp;</span><span>A</span><span>C</span><span>C</span><span>O</span><span>U</span><span>N</span><span>T</span> <span class="caret"></span></a>
						
							<ul class="dropdown-menu">
							<li><a href="view_profile.php" onclick="f1()" class="link link--yaku"><span>V</span><span>I</span><span>E</span><span>W</span><span>&nbsp;</span> <span>P</span><span>R</span><span>O</span><span>F</span><span>I</span><span>L</span><span>E</span></a></li>
								
								<li><a href="change_password.php" onclick="f2()" class="link link--yaku"><span>S</span><span>E</span><span>E</span><span>T</span><span>I</span><span>N</span><span>G</span></a></li>
							</ul>
						</li>
						
						<li><a href="#" class="dropdown-toggle link link--yaku" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span>M</span><span>E</span><span>S</span><span>S</span><span>A</span><span>G</span><span>E</span><span class="caret"></span></a>
						
						<ul class="dropdown-menu">
							<li><a href="compose_message.php" class="link link--yaku"><span>C</span><span>O</span><span>M</span><span>P</span><span>O</span><span>S</span><span>E</span><span>&nbsp;</span><span>M</span><span>E</span><span>S</span><span>S</span><span>A</span><span>E</span></a></li>
								
								<li><a href="inbox.php" class="link link--yaku"><span>I</span><span>N</span><span>B</span><span>O</span><span>X</span></a></li>
							</ul>
						</li>
						<li><a href="logout.php" class="link link--yaku"><span>L</span><span>O</span><span>G</span><span>O</span><span>U</span><span>T</span></a></li>
					</ul>		
					<div class="clearfix"> </div>
				</div><!-- //navigation --> 
			</div>	
		</nav>	
		
	</div>	
	<!-- //header -->