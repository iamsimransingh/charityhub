<?php require "header_files.php"; ?>
</head>
<body> 
	<!-- header -->
	<?php require "header.php"; ?>
	<!-- //header -->
	
	<!-- Our Projects -->
	<div class="gallery w3-agileits">  
		<div class="container">
			<h3 class="agileits-title1 w3-agileits">Our Projects</h3>   
			<div class="agileinfo-gallery-row">
				
				<!-- loop start -->
				
				<?php
					
					$a="enable";
					$q="select * from add_project where project_status='$a'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
						$pid=$r['project_id'];
				?>
				
					<div class="col-md-4 col-sm-4 col-xs-6 w3gallery-grids">
						<a href="view_project_bl.php?z=<?php echo $pid; ?>" class="imghvr-hinge-right wthree">
							<img src="admin/upload_image_project/<?php echo $r['project_image']; ?>" class="img-response" alt="" title="Kids Care Image" width="640px" height="200px"/> 
							<div class="agile-figcaption">
							  <h4><?php echo $r['project_name']; ?></h4>
							  <p><?php echo $r['project_description']; ?> </p>
							</div>
							
						</a> 
					</div> 
				<?php
					}
				?>
				<!-- loop end -->	
				
				<div class="clearfix"> </div>
				<script type="text/javascript" src="js/simple-lightbox.min.js"></script>
				
			</div> 
		</div>
	</div>
	<!-- //our projects --> 
	<!-- footer -->
	<?php require "footer.php"; ?>
	<!-- //footer -->  
	<!-- start-smooth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>	
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	
	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
</body>
</html>