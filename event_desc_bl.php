<?php require "header_files.php"; 
?>
<body> 
	<?php require "header.php"; ?>
	
	
	<!-- about -->
<?php
$eid=$_GET['eid'];
$q="select * from add_event where event_id='$eid'";
$c=$conn->query($q);
while($r=$c->fetch_assoc())
{
?>
	<div class="about w3-agileits">
		<div class="container"> 
			<div class="col-md-6 about-left" align="center">
				<img src="admin/upload_image_event/<?php echo $r['event_image']; ?>" height="200px" width="200px" class="img-circle" style="box-shadow:1px 1px 10px #00bcd4;" >
				<h3 class="agileits-title"><?php echo $r['event_name']; ?></h3> 
			</div>
			<div class="col-md-6 about-right agileits-w3layoutsleft">
				<h4><?php echo $r['event_name']; ?></h4>
				
				<p><?php echo $r['event_description']; ?></p>
				<p><b>Address: </b><?php echo $r['event_address']; ?></p>
				<ul> 
					<li><span class="glyphicon glyphicon-share-alt"></span> <b>Location: </b><?php echo $r['event_location']; ?></li>
					<li><span class="glyphicon glyphicon-share-alt"></span> <b>Date: </b><?php echo $r['event_sdate']; ?></li>
					<li><span class="glyphicon glyphicon-share-alt"></span> <b>Time: </b><?php echo $r['event_etime']; ?></li>
				</ul> 
			</div> 
			<div class="clearfix"> </div>
		</div>
	</div>
<?php
}
?>
	<!-- //about -->	 
	<?php require "footer.php"; ?>
</body>
</html>