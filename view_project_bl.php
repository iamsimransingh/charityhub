<?php require "header_files.php"; 
?>
<body> 
	<?php require "header.php"; ?>
<div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
			<div class="col-lg-3"></div>
                <div class="col-lg-8">
				<br><br>
                    <b style="font-family:Andalus; font-size:33px;text-align:center;">View Project</b><br><br>
                </div>
			<div class="col-lg-2"></div>
            </div>

            <!-- ... Your content goes here ... -->

			<div class="col-md-10">
			<table class="table">
				<?php
				
				$id=$_GET['z']; 
				if($id=="")
				{
					echo "<script>alert('Invalid ID'); window.location='project.php';</script>";
				}
				else
				{
					$q="select * from add_project where project_id='$id'";
					$chk=$conn->query($q);
					while($r=$chk->fetch_assoc())
					{
				?>
						
						<tr>
							
							<td rowspan="12" align="center">
							<a href="#" onclick="f1()" class="mybtn1">Raise Funds</a>
							<br>
							<!-- location code -->
							<div style="height:350px; width:350px;">
							 <?php 
								$source =  $r['project_latitute'].' , '.$r['project_longitute'];
							?>
							<iframe id="mapFrame" width="350px" height="250px" style="float:left;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?saddr=<?php echo $source; ?>+&amp;ie=UTF8&amp;z=12&amp;t=m&amp;output=embed"></iframe>
							</div>
							<!-- location code end-->
							</td>
							<th>Project Name</th>
							<td><?php echo $r['project_name']; ?></td>
							<td rowspan="12">
							<h4>Total Funds: <span style="color:red;">$
							<?php
							
							$q="select SUM(raised_fund) as tot from raise_funds where pid='$id'";
							$sum=$conn->query($q);
							while ($test = $sum->fetch_assoc()) 
							 {  echo $test['tot'];  }
							?>
							</span></h4>
							<br>
							<h4>Remaining Days:<span style="color:red;">
							<?php 
							$today = time();
							$remaining=strtotime($r['project_end_date'])- $today;
							$days_remaining=floor($remaining / 86400);
							 echo $days_remaining." days"; 
							?>
							</span>
							</h4><br>
							<img src="admin/upload_image_project/<?php echo $r['project_image']; ?>" height="250px" width="250px" class="img-thumbnail" style="box-shadow:1px 1px 10px #00bcd4;" >
							<br><br>
							<h2> <?php echo $r['project_name']; ?></h2>
							</td>
						</tr>
						
						<tr>
							<th>Start Date</th>
							<td><?php echo $r['project_date']; ?></td>
						</tr>
						
						<tr>
							<th>End Date</th>
							<td><?php echo $r['project_end_date']; ?></td>
						</tr>
						
						<tr>
							<th>Location</th>
							<td><?php echo $r['project_location']; ?></td>
						</tr>
						<tr>
							<th>Address</th>
							<td><?php echo $r['project_address']; ?></td>
						</tr>
						
						<tr>
							<th>Minimum Demand</th>
							<td>$<?php echo $r['project_mini']; ?></td>
						</tr>
						
						<tr>
							<th>Maximum Demand</th>
							<td>$<?php echo $r['project_maxi']; ?></td>
						</tr>
						
						<tr>
							<th>Phone No.</th>
							<td><?php echo $r['project_phone']; ?></td>
						</tr>
						
						<tr>
							<th>Description</th>
							<td><?php echo $r['project_description']; ?></td>
						</tr>
						
						<tr>
							<th>Uploaded By</th>
							<td><?php echo $r['project_upload_by']; ?></td>
						</tr>
						
						<tr>
							<th>Registered Time</th>
							<td><?php echo $r['time']; ?></td>
						</tr>
						
						
						
				<?php
					}
				}
				?>
			</table>
			</div>
			<div class="col-md-2"></div>
        </div>
    </div>
	<br><br><br><br>
	<?php require "footer.php"; ?>
</body>
</html>